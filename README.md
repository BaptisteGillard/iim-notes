iim-notes
=========

A Symfony project created on November 16, 2015, 3:05 pm.

# Getting started

$ git clone https://bitbucket.org/BaptisteGillard/iim-notes

go to the project directory

$ composer install

then check your app/config/parameters.yml, if not exist create it and set the database

create the database with

$ app/console doctrine:database:create

update the database with 

$ php app/console doctrine:schema:update --force

Then 

$ php app/console assets:install --symlink
