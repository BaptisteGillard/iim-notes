<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;


/**
* Class APIController
*/
class APIController extends FOSRestController
{
    /**
     * @Route("/restapi/students", name="restapi_students")
     */
    public function getStudentAction()
    {
        $datas = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Student')
            ->findAll();

        $view = $this->view($datas, 200);

        return $this->handleView($view);
    }

    /**
     * @Route("/restapi/student/{id}", name="restapi_student")
     */
    public function getOneStudentAction($id)
    {
        $datas = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Student')
            ->findOneById($id);

        $view = $this->view($datas, 200);

        return $this->handleView($view);
    }

    /**
     * @Route("/restapi/grades/{id}", name="restapi_grades")
     */
    public function getGradeAction($id)
    {
        $student = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Student')
            ->findOneById($id);



        $datas = $student->getGrades();

        $view = $this->view($datas, 200);

        return $this->handleView($view);
    }

    /**
     * @Route("/restapi/exams", name="restapi_exams")
     */
    public function getExamAction()
    {
        $datas = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Exam')
            ->findAll();

        $view = $this->view($datas, 200);

        return $this->handleView($view);
    }

    /**
     * @Route("/restapi/exam/{id}", name="restapi_exam")
     */
    public function getOneExamController($id)
    {
        $datas = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Exam')
            ->findOneById($id);

        $view = $this->view($datas, 200);

        return $this->handleView($view);
    }
}
